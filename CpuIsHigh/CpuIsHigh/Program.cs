﻿using System;

namespace CpuIsHigh
{
    class Program
    {
        static void Main(string[] args)
        {
            int pid = 18784;
            bool counter = false;
            bool flag = args != null && args.Length != 0;
            if (flag)
            {
                int.TryParse(args[0], out pid);
            }
            bool flag2 = args != null && args.Length > 1;
            if (flag2)
            {
                counter = Convert.ToBoolean(args[1]);
            }
            ProcessHelper.Start(pid, counter);
            // ProcessHelper.Start()
        }
    }
}
