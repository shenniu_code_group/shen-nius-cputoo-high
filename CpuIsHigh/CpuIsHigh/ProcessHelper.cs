﻿using Microsoft.Samples.Debugging.MdbgEngine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CpuIsHigh
{
    public class ProcessHelper
    {
        public static void Start(int pid, bool counter)
        {
            try
            {
                var process = Process.GetProcessById(pid);
                Console.WriteLine($"Id:{process.Id},ProcessName:{process.ProcessName}");
                List<MyThreadInfo> info;
                if (counter)
                {
                    info = GetPerformanceCounterInfo(process);//耗时间，而且没啥用
                    GetThreadInfo(process, info);
                }
                else
                {
                    info = GetThreadInfo(process);
                }

                info = GetCallStackInfo(process, info);
                CreateReport(process, info);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.Read();
            }
        }

        private static List<MyThreadInfo> GetPerformanceCounterInfo(Process process)
        {
            Console.WriteLine("正在统计计数器");
            var cate = new PerformanceCounterCategory("Thread");
            var instances = cate.GetInstanceNames().Where(instance =>
                instance.StartsWith(process.ProcessName, StringComparison.CurrentCultureIgnoreCase));
            var counters = instances.Select(instance =>
            {
                var counter1 =
                    new PerformanceCounter("Thread", "ID Thread", instance, true);
                var counter2 =
                    new PerformanceCounter("Thread", "% Processor Time", instance, true);
                return new MyThreadCounterInfo(counter1, counter2);
            }).ToList();

            var countersList = new List<MyThreadInfo>();
            foreach (var pair in counters)
                try
                {
                    //计数器信息
                    countersList.Add(new MyThreadInfo
                    {
                        Id = pair.IdCounter.NextValue().ToString(CultureInfo.InvariantCulture),
                        ProcessorTimePercentage =
                            pair.ProcessorTimeCounter.NextValue().ToString(CultureInfo.InvariantCulture)
                    });
                }
                catch
                {
                    //ignored
                }

            return countersList;
        }

        private static List<MyThreadInfo> GetThreadInfo(Process process, List<MyThreadInfo> countersList)
        {
            Console.WriteLine("正在统计线程信息");
            //线程信息
            var collection = process.Threads;
            foreach (ProcessThread thread in collection)
            {
                var info = countersList.FirstOrDefault(e => e.Id == thread.Id.ToString());
                try
                {
                    if (info != null)
                    {
                        info.UserProcessorTime = thread.UserProcessorTime;
                        info.StartTime = thread.StartTime;
                    }
                }
                catch
                {
                    // ignored
                }
            }

            return countersList;
        }

        private static List<MyThreadInfo> GetThreadInfo(Process process)
        {
            Console.WriteLine("正在统计线程信息");
            //线程信息
            var collection = process.Threads;

            var threadList = new List<MyThreadInfo>();
            foreach (ProcessThread thread in collection)
                threadList.Add(new MyThreadInfo
                {
                    CallStack = null,
                    Id = thread.Id.ToString(),
                    ProcessorTimePercentage = null,
                    StartTime = thread.StartTime,
                    UserProcessorTime = thread.UserProcessorTime
                });
            return threadList;
        }

        private static List<MyThreadInfo> GetCallStackInfo(Process process, List<MyThreadInfo> threadInfos)

        {
            Console.WriteLine("正在统计栈信息");
            var debugger = new MDbgEngine();
            MDbgProcess proc = null;
            try
            {
                proc = debugger.Attach(process.Id, MdbgVersionPolicy.GetDefaultAttachVersion(process.Id));
                DrainAttach(debugger, proc);

                var tc = proc.Threads;
                foreach (MDbgThread t in tc)
                {
                    var tempStrs = new StringBuilder();
                    foreach (var f in t.Frames) tempStrs.AppendFormat("<br>" + f);

                    var info = threadInfos.FirstOrDefault(e => e.Id == t.Id.ToString());
                    if (info != null)
                        info.CallStack = tempStrs.Length == 0 ? "no managment call stack" : tempStrs.ToString();
                }
            }
            finally
            {
                proc?.Detach().WaitOne();
            }

            return threadInfos;
        }

        private static void CreateReport(Process process, List<MyThreadInfo> info)
        {
            Console.WriteLine("正在创建报表");
            info = info.OrderByDescending(e => e.UserProcessorTime).ToList();
            var html =
                $@"<html>
                        <head>
                            <title>{process.ProcessName}</title>
                            <style type='text/css'>table, td{{border: 1px solid #000;border-collapse: collapse;}} </style>
                       </head>
                        <body>
                             {string.Join("<br>", info.Select(item =>
                    $@"<table style='width: 1000px;'>
                                        <tr><td style='width: 80px;'>ThreadId</td><td style='width: 200px;'>{item.Id}</td><td style='width: 140px;'>% Processor Time</td><td>{item.ProcessorTimePercentage}</td></tr>
                                        <tr><td style='width: 80px;'>UserProcessorTime</td><td style='width: 200px;'>{item.UserProcessorTime}</td><td style='width: 140px;'>StartTime</td><td>{item.StartTime}</td></tr>
                                        <tr><td colspan='4'>{item.CallStack}</td></tr>
                                    </table>"))}
                       </body>
                </html>";
            File.WriteAllText(process.ProcessName + ".htm", html);
            Process.Start(process.ProcessName + ".htm");
        }

        private static void DrainAttach(MDbgEngine debugger, MDbgProcess proc)
        {
            var fOldStatus = debugger.Options.StopOnNewThread;
            debugger.Options.StopOnNewThread = false; // skip while waiting for AttachComplete

            proc.Go().WaitOne();
            Debug.Assert(proc.StopReason is AttachCompleteStopReason);

            debugger.Options.StopOnNewThread = true; // needed for attach= true; // needed for attach

            // Drain the rest of the thread create events.
            while (proc.CorProcess.HasQueuedCallbacks(null))
            {
                proc.Go().WaitOne();
                Debug.Assert(proc.StopReason is ThreadCreatedStopReason);
            }

            debugger.Options.StopOnNewThread = fOldStatus;
        }

        internal class MyThreadCounterInfo
        {
            public PerformanceCounter IdCounter;
            public PerformanceCounter ProcessorTimeCounter;

            public MyThreadCounterInfo(PerformanceCounter counter1, PerformanceCounter counter2)
            {
                IdCounter = counter1;
                ProcessorTimeCounter = counter2;
            }
        }

        internal class MyThreadInfo
        {
            public string CallStack = "null";
            public string Id;
            public string ProcessorTimePercentage;
            public DateTime StartTime;
            public TimeSpan UserProcessorTime;
        }
    }
}
