# ShenNiusCPUTooHigh

#### 介绍
基于dotnet开发的控制台程序，用来排查windows程序CPU过高的原因。基于进程id来排查具体线程，最后到堆栈，直至具体的方法。生成友好的html页面，方便快速的定位出问题所在。

